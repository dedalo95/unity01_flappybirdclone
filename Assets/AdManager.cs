﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
public class AdManager : MonoBehaviour
{
    private string placementId = "3984985";
    private bool testMode = true;
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.Initialize(placementId,testMode);
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        
    }
    void Update(){
        if(Advertisement.IsReady()){
            Advertisement.Banner.Show(placementId);
        }
    }
}
