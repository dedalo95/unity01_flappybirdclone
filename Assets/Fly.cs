﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    public GameManager gameManager;
	public float velocity = 1;
	private Rigidbody2D rb;
    public AudioSource source;
    public AudioClip clip_volo;
    public AudioClip clip_morte_1;
    public AudioClip clip_morte_2;
    private bool soundPlayer = true;
    // Start is called before the first frame update
    void Start()
    {
        //acquisisco rigidbody
        rb = GetComponent<Rigidbody2D>();
    }

    // ad ogni frame
    void Update()
    {
		//se clicco col tasto sinistro del mouse (num 0)
        if(Input.GetMouseButtonDown(0)){
			//porto il rigidbody del componente verso l'alto attraverso il parametro velocità
			rb.velocity = Vector2.up*velocity;
            if(soundPlayer){
                source.PlayOneShot(clip_volo, 0.5f);
            }
		}
    }
    //ad ogni collisione
    private void OnCollisionEnter2D(Collision2D collision){
        soundPlayer = false;
        //dico al game manager di andare in gameover
        gameManager.GameOver();
        source.PlayOneShot(clip_morte_1, 0.5f);
        source.PlayOneShot(clip_morte_2, 0.5f);
    }
}
