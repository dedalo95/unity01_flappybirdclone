﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore : MonoBehaviour
{
    public AudioSource pointSound;
    public AudioClip clip;
   //Dopo la collisione
   private void OnTriggerEnter2D(Collider2D col){
       //incremento punteggio
       print(col.gameObject.tag);
       if(col.gameObject.tag=="Player"){
            Score.score++;
            pointSound.PlayOneShot(clip, 0.5f);
       }
   }
}
