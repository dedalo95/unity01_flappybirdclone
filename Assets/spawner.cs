﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
	public float maxTime = 1;
	public float timer = 0;
	public GameObject pipe;
	public float height;
    // Start is called before the first frame update
    void Start()
    {
		//istanzio i primi tubi
		//nuovo tubo
		GameObject newPipe = Instantiate(pipe);
		//imposto altezza random
		newPipe.transform.position = transform.position + new Vector3(0,Random.Range(-height, height),0);
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > maxTime){
			//istanzio nuova pipe
			GameObject newPipe = Instantiate(pipe);
			//imposto altezza random
			newPipe.transform.position = transform.position + new Vector3(0,Random.Range(-height, height),0);
			//distruggi dopo 15 secondi
			Destroy(newPipe,15);
			timer=0;
		}
		timer += Time.deltaTime;
    }
}
