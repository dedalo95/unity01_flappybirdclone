﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
   public GameObject gameOverCanvas;
   //all'avvio
   private void Start(){
       //il tempo scorre e il canva deve essere invisibile
       Time.timeScale=1;
       gameOverCanvas.SetActive(false);
   }
   //al gameover
   public void GameOver(){
       //devo mostrare il canva del game over e fermare il tempo
       gameOverCanvas.SetActive(true);
       Time.timeScale=0;
   }
   //Ricarico la scena
   public void Replay(){
       SceneManager.LoadScene(0);
   }
}
