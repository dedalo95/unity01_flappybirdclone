﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreController : MonoBehaviour
{
    public int highscore=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        highscore = PlayerPrefs.GetInt ("highscore", highscore);
        GetComponent<UnityEngine.UI.Text>().text = highscore.ToString();
    }
}
